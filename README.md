# Herramientas de preprocesamiento

## Instalacion del repositorio
Una vez clonado, se instalan las dependencias usando:
```
pip install requirements.txt
```
Preferentemente dentro de un [entorno virtual](https://docs.python.org/3/library/venv.html).

## Uso
Ejecutar el script `auto-deskew.py` desde una terminal con el siguiente comando:
```
python auto-deskew.py
```
El script abrirá un diálogo para seleccionar el directorio donde se encuentran las imágenes a procesar. Una vez seleccionado, se procesarán todas las imágenes que se encuentren en el mismo. El resultado se guardará en el subdirectorio `deskewed`.