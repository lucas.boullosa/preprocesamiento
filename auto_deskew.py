import math
from typing import Tuple, Union

import cv2
import numpy as np
import os
import tkinter.filedialog

from deskew import determine_skew


def rotate(
        image: np.ndarray, angle: float, background: Union[int, Tuple[int, int, int]]
) -> np.ndarray:
    old_width, old_height = image.shape[:2]
    angle_radian = math.radians(angle)
    width = abs(np.sin(angle_radian) * old_height) + abs(np.cos(angle_radian) * old_width)
    height = abs(np.sin(angle_radian) * old_width) + abs(np.cos(angle_radian) * old_height)

    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    rot_mat[1, 2] += (width - old_width) / 2
    rot_mat[0, 2] += (height - old_height) / 2
    return cv2.warpAffine(image, rot_mat, (int(round(height)), int(round(width))), borderValue=background)


#open file dialogue to select directory
root = tkinter.Tk()
indir = tkinter.filedialog.askdirectory()
#close the dialogue
root.destroy()

# rotate all images in a directory
for file in os.listdir(indir):
    if file.endswith(".jpg") or file.endswith(".jpeg") or file.endswith(".png") or file.endswith(".tif"):
        image = cv2.imread(indir +'/'+ file)
        grayscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        print('Enderezando imagen: ' + file)
        angle = determine_skew(grayscale)
        # creo que hay un bug con las imagenes rectas y devuelve un angulo de 45
        if abs(angle) == 45:
            angle = 0
        rotated = rotate(image, angle, (0, 0, 0))
        if os.path.exists(indir+'/deskewed/' + file):
            print('Ya existe el archivo')
            continue
        elif os.path.exists(indir+'/deskewed'):
            cv2.imwrite(indir+'/deskewed/' + file, rotated)
        else:
            os.mkdir(indir+'/deskewed')
            cv2.imwrite(indir+'/deskewed/' + file, rotated)
        continue
    else:
        continue


